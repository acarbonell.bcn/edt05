package com.example.edt05;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private ImageView img1;
    private LinearLayout text1;
    private Button bt1;
    private ImageView img2;
    private LinearLayout text2;
    private Button bt2;
    private ImageView img3;
    private LinearLayout text3;
    private Button bt3;
    private ImageView img4;
    private LinearLayout text4;
    private Button bt4;
    private ImageView img5;
    private LinearLayout text5;
    private Button bt5;



    private boolean count1 = true;
    private boolean count2 = true;
    private boolean count3 = true;
    private boolean count4 = true;
    private boolean count5 = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    img1 = findViewById(R.id.imageView);
    text1 = findViewById(R.id.text1);
    img2 = findViewById(R.id.imageView2);
    text2 = findViewById(R.id.text2);
    img3 = findViewById(R.id.imageView3);
    text3 = findViewById(R.id.text3);
    img4 = findViewById(R.id.imageView4);
    text4 = findViewById(R.id.text4);
    img5 = findViewById(R.id.imageView5);
    text5 = findViewById(R.id.text5);

    bt1 = findViewById(R.id.bt1);
    bt2 = findViewById(R.id.bt2);
    bt3 = findViewById(R.id.bt3);
    bt4 = findViewById(R.id.bt4);
    bt5 = findViewById(R.id.bt5);

    bt1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
            if(text1.getAlpha()==1){
                startActivity(intent);
            }
        }
    });
    bt2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
            if(text2.getAlpha()==1){
                startActivity(intent);
            }
        }
    });
    bt3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
            if(text3.getAlpha()==1){
                startActivity(intent);
            }
        }
    });
    bt4.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
            if(text4.getAlpha()==1){
                startActivity(intent);
            }
        }
    });
    bt5.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ReservarActivity.class);
            if(text5.getAlpha()==1){
                startActivity(intent);
            }
        }
    });



    img1.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimatorSet set;
            AnimatorSet set1;
            if (count1){
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimatortext);
                count1 = false;
            } else {
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimatortext);
                count1 = true;
            }
            set.setTarget(v);
            set1.setTarget(text1);
            AnimatorSet animationSet = new AnimatorSet();
            animationSet.playTogether(set, set1);
            animationSet.start();

        }
    });

    img2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimatorSet set;
            AnimatorSet set1;
            if (count2){
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimatortext);
                count2 = false;
            } else {
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimatortext);
                count2 = true;
            }
            set.setTarget(v);
            set1.setTarget(text2);
            AnimatorSet animationSet = new AnimatorSet();
            animationSet.playTogether(set, set1);
            animationSet.start();

        }
    });

    img3.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimatorSet set;
            AnimatorSet set1;
            if (count3){
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimatortext);
                count3 = false;
            } else {
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimatortext);
                count3 = true;
            }
            set.setTarget(v);
            set1.setTarget(text3);
            AnimatorSet animationSet = new AnimatorSet();
            animationSet.playTogether(set, set1);
            animationSet.start();

        }
    });

    img4.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimatorSet set;
            AnimatorSet set1;
            if (count4){
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimatortext);
                count4 = false;
            } else {
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimatortext);
                count4 = true;
            }
            set.setTarget(v);
            set1.setTarget(text4);
            AnimatorSet animationSet = new AnimatorSet();
            animationSet.playTogether(set, set1);
            animationSet.start();

        }
    });

    img5.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AnimatorSet set;
            AnimatorSet set1;
            if (count5){
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.frontanimatortext);
                count5 = false;
            } else {
                set = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimator);
                set1 = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this, R.animator.backanimatortext);
                count5 = true;
            }
            set.setTarget(v);
            set1.setTarget(text5);
            AnimatorSet animationSet = new AnimatorSet();
            animationSet.playTogether(set, set1);
            animationSet.start();

        }
    });


    }
}